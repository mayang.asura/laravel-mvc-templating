<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        
        return view('pages.register');
    }

    public function welcome(Request $request){

        // dd($request);
        $data = [
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'gender' => $request['gender'],
            'nationality' => $request['nationality'],
            'languange' => $request['languange'],
            'bio' => $request['bio'],
        ];
        
        return view('pages.welcome', $data);
    }
}
