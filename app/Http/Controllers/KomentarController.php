<?php

namespace App\Http\Controllers;

use App\Models\Komentar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class KomentarController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'point'   => ['required', 'numeric'],
            'content' => 'required',
            'film_id' => ['required', 'numeric']
        ]);

        Komentar::create([
            'point'     => $request->point,
            'content'   => $request->content,
            'user_id'   => Auth::id(),
            'film_id'   => $request->film_id
        ]);

        return redirect('film/'.$request->film_id);
    }
}
