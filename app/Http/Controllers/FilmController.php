<?php

namespace App\Http\Controllers;

use App\Models\Film;
use App\Models\Genre;
use Illuminate\Http\Request;
use File;

class FilmController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
    }    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $films = Film::all();
        return view('pages.film.index', ['films' => $films]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $genres = Genre::all();
        return view('pages.film.create', compact('genres'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required',
            'ringkasan' => 'required',
            'year' => ['required', 'numeric'],
            'poster' => ['required', 'image', 'mimes:jpg,png,jpeg', 'max:2048'],
            'genre_id' => ['required', 'numeric']
        ]);

        $filename = time().'.'. $request->poster->extension();

        $request->poster->move(public_path('images'), $filename);

        // Film::create($request->all());
        $film = new Film;
        $film->judul = $request->judul;
        $film->ringkasan = $request->ringkasan;
        $film->year = $request->year;
        $film->genre_id = $request->genre_id;
        $film->poster = $filename;

        $film->save();


        return redirect('film')->with('success', 'Film berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = Film::find($id);
        return view('pages.film.detail', compact('film'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $film = Film::find($id);
        $genres = Genre::all();

        return view('pages.film.edit', compact('film', 'genres'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'judul' => 'required',
            'ringkasan' => 'required',
            'year' => ['required', 'numeric'],
            'genre_id' => ['required', 'numeric'],                                    
            'poster' => ['image', 'mimes:jpg,png,jpeg', 'max:2048'],                         //upload file misalnya tidak required
        ]);

        $film = Film::find($id);
        if($request->has('poster')){
            $path = 'images/';

            File::delete($path . $film->poster);

            $filename = time().'.'. $request->poster->extension();
            $request->poster->move(public_path('images'), $filename);
            $film->poster = $filename;
        }

        $film->judul = $request->judul;
        $film->ringkasan = $request->ringkasan;
        $film->year = $request->year;
        $film->genre_id = $request->genre_id;

        $film->update();

        return redirect('film')->with('success', 'Film berhasil diupdate!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $film = Film::find($id);
        $film->delete();
        return redirect('film')->with('success', 'Film berhasil dihapus!');
    }

    public function delete($id){
        
        $film = Film::find($id);

        return view('pages.film.delete', compact('film'));
    }
}
