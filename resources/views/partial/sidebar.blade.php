<div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <h1>MY</h1>
        {{-- <img src="{{asset('admin/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image"> --}}
      </div>
      @auth
      <div class="info">
        <a href="#" class="d-block">{{Auth::user()->name}} </a>
      </div>
      @else
      <div class="info">
        <a href="#" class="d-block">Nama Akun</a>
      </div>
      @endauth
    </div>

    <!-- SidebarSearch Form -->
    <div class="form-inline">
      <div class="input-group" data-widget="sidebar-search">
        <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-sidebar">
            <i class="fas fa-search fa-fw"></i>
          </button>
        </div>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->

        <li class="nav-item">
          <a href="/" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Dashboard
              <span class="right badge badge-danger">New</span>
            </p>
          </a>
        </li>

        @guest
        <li class="nav-item">
          <a href="/film" class="nav-link {{(request()->segment(1)=='film')?'active':''}}">
            <i class="nav-icon fas fa-solid fa-film"></i>
            <p>
              Film
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="/genre" class="nav-link {{(request()->segment(1)=='genre')?'active':''}}">
            <i class="nav-icon fas fa-solid fa-star"></i>
            <p>
              Genre
            </p>
          </a>
        </li>
        @endguest

        @auth
        <li class="nav-item {{(request()->segment(1)=='genre')?'menu-is-opening menu-open':''}}">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-th"></i>
            <p>
              Table
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/table" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Table</p>
              </a>
            </li>
            <li class="nav-item {{(request()->segment(1)=='data-table')?'active':''}}">
              <a href="/data-table" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Data Table</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item {{(request()->segment(1)=='cast')?'menu-is-opening menu-open':''}}">
          <a href="#" class="nav-link {{(request()->segment(1)=='cast')?'active':''}}">
            <i class="nav-icon fas fa-regular fa-universal-access"></i>
            <p>
              Cast
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/cast" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Cast</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="/cast/create" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Tambah Cast</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item {{(request()->segment(1)=='genre')?'menu-is-opening menu-open':''}}">
          <a href="#" class="nav-link {{request()->segment(1)=='genre'?'active':''}}">
            <i class="nav-icon fas fa-solid fa-star"></i>
            <p>
              Genre
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/genre" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Genre</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="/genre/create" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Tambah Genre</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item {{(request()->segment(1)=='film')?'menu-is-opening menu-open':''}}">
          <a href="#" class="nav-link {{(request()->segment(1)=='film')?'menu-is-opening menu-open':''}}">
            <i class="nav-icon fas fa-solid fa-film"></i>
            <p>
              Film
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/film" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Film</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="/film/create" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Tambah Film</p>
              </a>
            </li>
          </ul>
        </li>
        @endauth
        
        {{-- LOGOUT --}}
        @auth
        <li class="nav-item">
            <a class="nav-link bg-danger" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();">
                Logout
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </li>
        @endauth

        @guest
        <li class="nav-item">
          <a class="nav-link bg-primary" href="{{ route('login') }}">
              Login
          </a>
          <form id="logout-form" action="{{ route('login') }}" method="POST" class="d-none">
              @csrf
          </form>
        </li>
        @endguest
    
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>