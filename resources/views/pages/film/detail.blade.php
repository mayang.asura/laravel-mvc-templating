@extends('layout.master')

@section('content')

<div class="card">
    <div class="card-header">
        <h3>Detail Film</h3>        
    </div>
    <div class="card-body">
        <div class="row">
            {{-- <div class="col-sm-12">Poster</div> --}}
            <div class="col-sm-12 mb-3">
                <img src="{{asset('images/'.$film->poster)}}" alt="" width="60%">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2">Nama</div>
            <div class="col-sm-9">: {{$film->judul}} </div>
        </div>
        <div class="row">
            <div class="col-sm-2">Umur</div>
            <div class="col-sm-9">: {{$film->year}} </div>
        </div>
        <div class="row">
            <div class="col-sm-2">Genre</div>
            <div class="col-sm-9">: {{$film->genre->nama}} </div>
        </div>
        <div class="row">
            <div class="col-sm-2">Bio</div>
            <div class="col-sm-9">: {{$film->ringkasan}} </div>
        </div><hr>

        <div class="row">
            <div class="col-sm-12">
                <h2>Komentar:</h2>
                @forelse ($film->komentar as $item)
                <div class="media border my-2">
                    <h1 class="mr-3 ml-2 my-1">{{$item->point}}</h1>
                    <div class="media-body">
                      <h5 class="mt-1 text-info">{{$item->user->name}}</h5>
                      <p>{{$item->content}}</p>
                    </div>
                </div>
                    
                @empty
                    <p>Tidak ada komentar</p>
                @endforelse
                <form action="/komentar" method="post" class="my-5">
                    @csrf
                    <input type="hidden" name="film_id" value="{{$film->id}}">
                    <div class="form-group">
                        <select name="point" id="point" class="form-control form-control-sm">
                            <option value="">--Pilih Point--</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                    @error('point')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                    @enderror
                    <div class="form-group">
                        <textarea name="content" id="" cols="30" rows="5" class="form-control form-control-sm">{{old('content')}} </textarea>
                    </div>
                    @error('content')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                    @enderror
                    <button type="submit" class="btn btn-sm btn-primary">Kirim Komentar</button>
                </form>
            </div>
        </div>
        <a href="/film" class="btn btn-sm btn-block btn-secondary my-5"> Kembali</a>
    </div>
</div>
    
@endsection