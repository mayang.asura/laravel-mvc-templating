{{-- !-- Delete Body -->  --}}
<form action="{{ route('film.destroy', $film->id) }}" method="post">
    <div class="modal-body">
        @csrf
        @method('DELETE')
        <h5 class="text-center">Anda yakin menghapus {{ $film->judul }} ini?</h5>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Tidak</button>
        <button type="submit" class="btn btn-sm btn-danger">Ya, Hapus</button>
    </div>
</form>