@extends('layout.master')

@section('content')

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Tambah Film</h3>
        </div>
        <div class="card-body">
            <form action="/film" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label>Judul</label>
                    <input type="text" name="judul" id="judul" value="{{old('judul')}}" class="form-control form-control-sm">
                    @error('judul')
                        <div class="alert alert-danger">
                            {{$message}}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Year</label>
                    <input type="text" name="year" id="year" value="{{old('year')}}" class="form-control form-control-sm">
                    @error('year')
                        <div class="alert alert-danger">
                            {{$message}}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Ringkasan</label>
                    <textarea name="ringkasan" id="ringkasan" cols="30" rows="10" class="form-control form-control-sm">{{old('ringkasan')}} </textarea>
                    @error('ringkasan')
                        <div class="alert alert-danger">
                            {{$message}}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Genre</label>
                    <select name="genre_id" id="genre_id" class="form-control form-control-sm">
                        <option value="">--Pilih Genre--</option>
                        @forelse ($genres as $item)
                            <option value="{{$item->id}}" {{$item->id==old('genre_id')?'selected':''}}>{{$item->nama}} </option>
                        @empty
                            <option value="">Tidak ada data</option>
                        @endforelse
                    </select>
                    @error('genre_id')
                        <div class="alert alert-danger">
                            {{$message}}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Poster</label>
                    <input type="file" name="poster" id="poster" value="{{old('poster')}}" class="form-control form-control-sm">
                    @error('poster')
                        <div class="alert alert-danger">
                            {{$message}}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-sm btn-primary">Simpan</button>
            </form>
        </div>
    </div>
    
@endsection