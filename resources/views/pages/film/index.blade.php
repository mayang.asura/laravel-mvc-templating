@extends('layout.master')

@section('content')

@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">List Film</h3>
        </div>
        <div class="card-body">
            <div class="row">
                @forelse ($films as $item)

                    <div class="col-sm-4">
                        <div class="card" style="width: 18rem;">
                            <img src="/images/{{$item->poster}}" class="card-img-top" alt="">
                            <div class="card-body">
                              <h5 class="card-title text-info">{{$item->judul}} </h5> <br> 
                              <span class="badge badge-pill badge-dark">{{$item->genre->nama}} </span>
                              <span class="badge badge-pill badge-dark">{{$item->year}} </span>
                              <hr>
                              <p class="card-text">{{Str::limit($item->ringkasan, 20)}} </p>
                              <div class="row my-2">
                                @guest
                                <a href="/film/{{$item->id}}" class="btn btn-sm btn-primary btn-block mx-1"> Detail</a>
                                @endguest
                                @auth
                                <a href="/film/{{$item->id}}" class="btn btn-sm btn-primary mx-1"> Detail</a>
                                <a href="/film/{{$item->id}}/edit" class="btn btn-sm btn-warning mx-1">Edit</a>
                                <a class="btn btn-sm btn-danger" data-toggle="modal" id="buttonDelete" data-target="#deleteModal" data-attr="{{ route('film.delete', $item->id) }}" title="Delete Cast">
                                    Delete
                                </a>
                                @endauth
                              </div>
                            </div>
                        </div>
        
                    </div>
                    
                    
                    @empty
                    Tidak ada list film
                    @endforelse
                </div>
        </div>
    </div>

    <!-- delete modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="smallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="deleteBody">
                    <div>
                        {{-- MODAL FORM DELETE --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection

@push('script')

<script>
    
    $(document).on('click', '#buttonDelete', function(event) {
        event.preventDefault();
        let href = $(this).attr('data-attr');
        console.log(href)
        $.ajax({
            url: href, 
            success: function(result) {
                $('#deleteModal').modal("show");
                $('#deleteBody').html(result).show();
            }, 
            // complete: function() {
            //     $('#loader').hide();
            // }, 
            error: function(jqXHR, testStatus, error) {
                console.log(error);
                alert("Page " + href + " cannot open. Error:" + error);
                $('#loader').hide();
            }, 
            timeout: 8000
        })
    });

</script>
    
@endpush