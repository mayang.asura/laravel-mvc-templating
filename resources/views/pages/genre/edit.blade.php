@extends('layout.master')

@section('content')

    <div class="card">
        <div class="card-header">
            <div class="card-title">

            </div>
        </div>
        <div class="card-body">
            <form action="/genre/{{$genre->id}}" method="post">
                @csrf
                @method('PUT')
                <div class="form-group row">
                    <div class="col-sm-6">
                        <label>Nama</label>
                        <input type="text" name="nama" id="nama" value="{{$genre->nama}}" class="form-control form-control-sm">
                        @error('nama')
                            <div class="alert alert-danger">
                                {{$message}}
                            </div>
                        @enderror
    
                    </div>
                </div>
                <button type="submit" class="btn btn-sm btn-primary">Simpan</button>
            </form>
        </div>
    </div>
    
@endsection