@extends('layout.master')

@section('content')

<div class="card">
    <div class="card-header">
        <h3 class="card-title">Tambah Genre</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <form action="/genre" method="post">
            @csrf
            <div class="form-group row">
                <div class="col-sm-6">
                    <label>Nama</label>
                    <input type="text" name="nama" id="nama" class="form-control form-control-sm">
                    @error('nama')
                        <div class="alert alert-danger">
                            {{$message}}
                        </div>
                    @enderror

                </div>
            </div>
            <button type="submit" class="btn btn-sm btn-primary">Simpan</button>
        </form>
    </div>
</div>
    
@endsection