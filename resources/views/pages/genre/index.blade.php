@extends('layout.master')

@section('content')

@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif

<div class="card">
    <div class="card-header">
        <h3 class="card-title">List Genre</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <table id="genreTable" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th colspan="2">Nama Genre</th>
                </tr>
            </thead>
            <tbody>

                @forelse ($genres as $key => $item)
                <tr>
                    <td>{{$key+1}} </td>
                    <td>{{$item->nama }} </td>
                    <td>
                        <a href="{{route('genre.show', $item->id)}} " class="btn btn-sm btn-info">Detail</a>
                        @auth
                        <a href="{{route('genre.edit', $item->id)}} " class="btn btn-sm btn-warning">Edit</a>
                        <a href="" class="btn btn-sm btn-danger" id="deleteButton" data-toggle="modal" data-target="#deleteModal" data-attr="{{route('genre.delete', $item->id)}} ">
                            Hapus
                        </a>
                        @endauth
                    </td>
                </tr>
                    
                @empty

                <tr>
                    <td colspan="3"><i>Tidak ada genre</i></td>
                </tr>
                    
                @endforelse

            </tbody>
            <tfoot>

            </tfoot>
        </table>
        {{-- <div class="row">
            @forelse ($genres as $item)
    
            <div class="card col-sm-4 mx-1" style="width: 18rem;">
                <div class="card-body">
                <h5 class="card-title">{{$item->nama}} </h5> 
                <br> <hr>
                <div class="row">
                    <a href="/genre/{{$item->id}}" class="btn btn-sm btn-primary mx-1"> Detail</a>
                    <a href="/genre/{{$item->id}}/edit" class="btn btn-sm btn-warning mx-1"> Edit</a>
                    <a class="btn btn-sm btn-danger" data-toggle="modal" id="buttonDelete" data-target="#deleteModal" data-attr="{{ route('delete', $item->id) }}" title="Delete Cast">
                        Delete
                        <i class="fas fa-trash text-danger fa-lg"></i>
                    </a>
                </div>
                </div>
            </div>

            @empty
            
            <i>Tidak ada list genre</i>
            
            @endforelse
        </div> --}}
    </div>
</div>

{{-- DELETE MODAL --}}

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="smallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="deleteBody">
                <div>
                    {{-- MODAL FORM DELETE --}}
                </div>
            </div>
        </div>
    </div>
</div>
    
@endsection

@push('script')

<script>
    // display a modal (small modal)
    $(document).on('click', '#deleteButton', function(event) {
        event.preventDefault();
        let href = $(this).attr('data-attr');
        console.log(href)
        $.ajax({
            url: href, 
            // beforeSend: function() {
            //     $('#loader').show();
            // },
            // return the result
            success: function(result) {
                $('#deleteModal').modal("show");
                $('#deleteBody').html(result).show();
            }, 
            // complete: function() {
            //     $('#loader').hide();
            // }, 
            error: function(jqXHR, testStatus, error) {
                console.log(error);
                alert("Page " + href + " cannot open. Error:" + error);
                $('#loader').hide();
            }, 
            timeout: 8000
        })
    });

</script>
    
@endpush