@extends('layout.master')

@section('content')

<div class="card">
    <div class="card-header">
        <h3>Detail Genre</h3>        
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-3">Nama</div>
            <div class="col-sm-9">: {{$genre->nama}} </div>
        </div>
        <div class="row">
            <div class="col-sm-3">Film ({{count($genre->films)}})</div>
            <div class="col-sm-9">: 
                <ul>
                    @forelse ($genre->films as $item)
                    <li><a href="/film/{{$item->id}}">{{ $item->judul}}</a></li>
                    @empty
                    <li></li>
                    @endforelse
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">Dibuat pada</div>
            <div class="col-sm-9">: {{ $genre->created_at->format('d/m/Y')}} </div>
        </div>
        <a href="/genre" class="btn btn-sm btn-block btn-secondary my-5">Kembali</a>
    </div>
</div>
    
@endsection