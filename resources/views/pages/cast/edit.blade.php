@extends('layout.master')

@section('content')

<div class="card">
    <div class="card-header">
        <h3 class="card-title">Edit Cast</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <form action="/cast/{{$cast->id}}" method="post">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label>Nama</label>
                <input type="text" name="nama" id="nama" value="{{$cast->nama}}" class="form-control form-control-sm">
                @error('nama')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Umur</label>
                <input type="text" name="umur" id="umur" value="{{$cast->umur}}" class="form-control form-control-sm">
                @error('umur')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Bio</label>
                <textarea name="bio" id="bio" cols="30" rows="10" class="form-control form-control-sm">{{$cast->bio}}
                </textarea>
                @error('bio')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-sm btn-block btn-primary">Simpan</button>
        </form>
    </div>
</div>
    
@endsection