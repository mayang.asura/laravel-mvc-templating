@extends('layout.master')

@section('content')

<div class="card">
    <div class="card-header">
        <h3 class="card-title">List Cast</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="row">
        @forelse ($casts as $item)

            <div class="col-sm-4">
                <div class="card" style="width: 18rem;">
                    <div class="card-body">
                      <h5 class="card-title">{{$item->nama}} </h5> (<span>{{ $item->umur}} </span>)<br> <hr>
                      <p class="card-text">{{$item->bio}} </p>
                      <div class="row">
                        @guest
                        <a href="/cast/{{$item->id}}" class="btn btn-sm btn-primary btn-block mx-1"> Detail</a>
                        @endguest
                        @auth
                        <a href="/cast/{{$item->id}}" class="btn btn-sm btn-primary mx-1"> Detail</a>
                        <a href="/cast/{{$item->id}}/edit" class="btn btn-sm btn-warning mx-1"> Edit</a>
                        <a class="btn btn-sm btn-danger" data-toggle="modal" id="buttonDelete" data-target="#deleteModal" data-attr="{{ route('delete', $item->id) }}" title="Delete Cast">
                             Delete
                            {{-- <i class="fas fa-trash text-danger fa-lg"></i> --}}
                        </a>
                        {{-- <form action="/cast/{{$item->id}}" method="post">
                          @csrf
                          @method('DELETE')
                          <button type="submit" class="btn btn-sm btn-danger mx-1">Delete</button>
          
                          </form> --}}
                            
                        @endauth
        
                      </div>
                    </div>
                </div>

            </div>
            
            
            @empty
            Tidak ada list cast
            @endforelse
        </div>
    
    </div>
</div>

<!-- delete modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="smallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="deleteBody">
                <div>
                    {{-- MODAL FORM DELETE --}}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('script')

    <script>
        // display a modal (small modal)
        $(document).on('click', '#buttonDelete', function(event) {
            event.preventDefault();
            let href = $(this).attr('data-attr');
            console.log(href)
            $.ajax({
                url: href, 
                // beforeSend: function() {
                //     $('#loader').show();
                // },
                // return the result
                success: function(result) {
                    $('#deleteModal').modal("show");
                    $('#deleteBody').html(result).show();
                }, 
                // complete: function() {
                //     $('#loader').hide();
                // }, 
                error: function(jqXHR, testStatus, error) {
                    console.log(error);
                    alert("Page " + href + " cannot open. Error:" + error);
                    $('#loader').hide();
                }, 
                timeout: 8000
            })
        });

    </script>
    
@endpush