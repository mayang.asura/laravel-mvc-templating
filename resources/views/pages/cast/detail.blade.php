@extends('layout.master')

@section('content')

<div class="card">
    <div class="card-header">
        <h3>Detail Cast</h3>        
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-3">Nama</div>
            <div class="col-sm-9">: {{$cast->nama}} </div>
        </div>
        <div class="row">
            <div class="col-sm-3">Umur</div>
            <div class="col-sm-9">: {{$cast->umur}} </div>
        </div>
        <div class="row">
            <div class="col-sm-3">Bio</div>
            <div class="col-sm-9">: {{$cast->bio}} </div>
        </div>
    </div>
</div>
    
@endsection