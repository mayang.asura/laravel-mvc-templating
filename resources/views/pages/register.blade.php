@extends('layout.master')

@section('title')
    Register
@endsection

@section('content')

    <h1>Buat Account Baru!</h1>

    <form action="/welcome" method="POST">
        @csrf

        <div>
            <label>First Name:</label><br><br>
            <input type="text" name="first_name" required>
        </div><br>
        
        <div>
            <label>Last Name:</label><br><br>
            <input type="text" name="last_name" required>
        </div><br>
        
        <div>
            <label>Gender:</label><br><br>
            <label><input type="radio" name="gender" value="male">Male</label><br>
            <label><input type="radio" name="gender" value="female">Female</label>
        </div><br>

        <div>
            <label>Nationality:</label><br><br>
            <select name="nationality" required>
                <option value="Indonesia">Indonesian</option>
                <option value="Australia">Australian</option>
                <option value="Malaysia">Malaysian</option>
            </select>
        </div><br>
        
        <div>
            <label>Languange Spoken:</label><br><br>
            <label><input type="checkbox" name="languange" value="bhs_indonesia" >Bahasa Indonesia</label><br>
            <label><input type="checkbox" name="languange" value="english" >English</label><br>
            <label><input type="checkbox" name="languange" value="others" >Other</label>
        </div><br>

        <div>
            <label>Bio:</label><br><br>
            <textarea name="bio" cols="30" rows="10" required></textarea>
        </div><br>

        <div>
            <button type="submit" value="Sign Up">Sign Up</button>
        </div>
    </form>
    
@endsection
