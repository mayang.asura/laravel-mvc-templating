--Model--

Game.php

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model{

           protected $fillable = ['name', 'gameplay', 'developer', 'year'];

}


--Controller--

GameController.php

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Game;

class GameController extends Controller{

public function index(Request $request){

      $games = Game::all();
      return view('pages.index', compact('games'));

}

public function show($id){

      $games = Game::find($id);
      return view('pages.show', compact('games'));

}

public function create(){

      return view('pages.create');

}

public function store(Request $request){

      $this->validate($request, [
         'name' => ['required', 'maks:45'],
         'gameplay' => 'required',
         'developer' => ['required', 'maks:45'],
         'year' => ['required', 'numeric'],
      ]);

      Game::create([
         'name' => $request->name,
         'gameplay' => $request->gameplay,
         'developer' => $request->developer,
         'year' => $request->year,
      ]) ;    
      
      return redirect('/games');

}

public function edit($id){

       $games = Game::find($id);
       return view('pages.edit', compact('games'));

}

public function update($id, Request $request){

       $this->validate($request, [
         'name' => ['required', 'maks:45'],
         'gameplay' => 'required',
         'developer' => ['required', 'maks:45'],
         'year' => ['required', 'numeric'],
      ]);

      $games = Game::find($id);
      
      $games->name = $request->name;
      $games->gameplay = $request->gameplay;
      $games->developer = $request->developer;
      $games->year = $request->year;
      $games->update();

      return redirect('/games');

}

public function destroy($id){
       $games = Game::find($id);
       $games->delete();

       return redirect('/games');
}

}



--Route--

web.php

<?php

// Code disini

Route::get('/games', [GameController::class, 'index']);
Route::get('/games/{id}', [GameController::class, 'show']);

Route::get('/games/create', [GameController::class, 'create']);
Route::post('/games', [GameController::class, 'store']);

Route::get('games/{id}/edit', [GameController::class, 'edit']);
Route::put('/games/{id}', [GameController::class, 'update']);

Route::delete('/games/{id}', [GameController::class, 'destroy']);






