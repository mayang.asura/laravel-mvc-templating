<!doctype html>

<html lang="en">

<head>

<!-- Required meta tags -->

<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">

<title>Edit Data</title>

</head>

<body>

<h2>Edit Data Game</h2>

//Code disini

<form action="/games/{{$games->id}} " method="post">
    @method('PUT')
    @csrf
        <div class="row">
            <div class="form-group">
                <label>Name</label>
                <input type="text" name="name" id="name" value="{{$games->name}}" class="form-control form-control-sm">
                @error('name')
                <div class="alert alert-danger">
                    {{$message}}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Game Play</label>
                <input type="text" name="gameplay" id="gameplay" value="{{$games->gameplay}}" class="form-control form-control-sm">
                @error('gameplay')
                <div class="alert alert-danger">
                    {{$message}}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Developer</label>
                <input type="text" name="developer" id="developer" value="{{$games->developer}}" class="form-control form-control-sm">
                @error('developer')
                <div class="alert alert-danger">
                    {{$message}}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Year</label>
                <input type="number" name="year" id="year" value="{{$games->year}}" class="form-control form-control-sm">
                @error('year')
                <div class="alert alert-danger">
                    {{$message}}
                </div>
                @enderror
            </div>
    
            <button type="submit" class="btn btn-sm btn-block btn-primary">Simpan</button>
        </div>
    
    </form>




<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>

</body>

</html>