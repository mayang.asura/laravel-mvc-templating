<!doctype html>

<html lang="en">

<head>

<!-- Required meta tags -->

<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">

<title>Detail Data</title>

</head>

<body>

<h2>Detail Data Game</h2>

//Code disini

<div class="row">
    <div class="col-sm-3">
        <h5>Name</h5>
    </div>
    <div class="col-sm-9">
        : {{$games->name}}
    </div>
</div>
<div class="row">
    <div class="col-sm-3">
        <h5>Gameplay</h5>
    </div>
    <div class="col-sm-9">
        : {{$games->gameplay}}
    </div>
</div>
<div class="row">
    <div class="col-sm-3">
        <h5>Developer</h5>
    </div>
    <div class="col-sm-9">
        : {{$games->developer}}
    </div>
</div>
<div class="row">
    <div class="col-sm-3">
        <h5>Year</h5>
    </div>
    <div class="col-sm-9">
        : {{$games->year}}
    </div>
</div>




<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>

</body>

</html>