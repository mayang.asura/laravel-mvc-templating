<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\KomentarController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexController::class, 'index']);
Route::get('/register', [AuthController::class, 'register']);
Route::post('/welcome', [AuthController::class, 'welcome']);

Route::middleware(['auth'])->group(function(){

    Route::get('/data-table', [IndexController::class, 'datatable']);

});
// Route::get('/master', function(){

//     return view('layout.master');
// });
// Route::get('/', function () {
//     return view('welcome');
// });


Route::resource('cast', CastController::class);
Route::resource('film', FilmController::class);
Route::resource('genre', GenreController::class);
Route::resource('film', FilmController::class);

Route::middleware(['auth'])->group(function(){
    Route::get('cast/delete/{id}', [CastController::class, 'delete'])->name('delete');
    Route::get('genre/delete/{id}', [GenreController::class, 'delete'])->name('genre.delete');
    Route::get('film/delete/{id}', [FilmController::class, 'delete'])->name('film.delete');
    Route::post('komentar', [KomentarController::class, 'store']);
});

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');